# TODO list

 - [x] Update README

 - [ ] Find a free plataform for code hospedating

 - [x] Write a README

 - [ ] Write CI file

 - [ ] Apply GPLv3

 - [x] Write makefile

 - [x] Fix main

 - Implement Caesar's cipher

	- [x] Text mode
		- [x] Case ignoring

	- [x] Stream mode

	- [x] Debug

	- [ ] Test everything

 - Implement frequency analisys

	- [x] Text mode

	- [x] Stream mode

	- [x] Debug

	- [ ] Test everything

 - Implement monoalfabetic subtitutions

	- [x] Text mode

	- [x] Stream mode

	- [x] Debug

	- [ ] Text everything

 - Implement binary conversion

	- [ ] Pure mode (00101010)

	- [ ]  BCD mode (1000 0010)

	- [ ] Gray mode (from pure, 00111111)

 - Implement n-ary conversion

# Code optimizations

General code:

 - [ ] Remove macro definition's reptition

Caesar's cipher:

 - [x] Rewrite for entire text, rather than one letter at time.

 - [x] Rewrite merging lib/mon.h:substitute() and caesars rotation

Frequency Analisys:

 - [ ] Remove code repetition.