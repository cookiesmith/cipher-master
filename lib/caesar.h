#ifndef GENLIB
#include "./genlib.h"
#define GENLIB
#endif

void caesar (char * key, int rot, char stream)
{
	int lrange, maxrot, hrange;
	if (stream) //In stream mode, every character is rotated, ordered by ascii table
	{
		lrange = 0;
		maxrot = 128;

	}
	else //In text mode, just a to z chars are rotated
	{
		lrange = 97;
		maxrot = 26;
	}

	rot = rot%maxrot; //Force rot to become lesser than maxrot
	hrange = lrange+maxrot; //Higher limit of rotation

	if (rot < 0) //Rotting n position backwards is as same as rotating maxrot-rot forwards
	{
		rot = maxrot+rot;
	}
	for (int i = 0; i < maxrot; i++)
	{
		if (lrange + i + rot < hrange) //If character isn't bigger than the biggest character, no problems
		{
			key[i] = lrange+i+rot;
		}
		else //In this case, we continue rotating from the init
		{
			key[i] = lrange+(lrange + i + rot - hrange);
		}
	}
}