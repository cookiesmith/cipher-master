# Cipher-master

This is a personal project, named as it is, for the purpose of dealing with multiple cripto operations.

## Implemented funtions:

 - [Caesar's cipher](https://en.wikipedia.org/wiki/Caesar%27s_cipher):
It work in stream mode, switching every character as it is the ASCII table, or text only mode;

 - [Frequency analysis](https://en.wikipedia.org/wiki/Frequency_analysis):
It work in stream mode, counting every character as it is the ASCII table, or text only mode;

 - [Monoalfabetic substitution](https://pt.wikipedia.org/wiki/Cifra_de_substitui%C3%A7%C3%A3o_monoalfab%C3%A9tica)
It work in stream mode, switching every character as it is the ASCII table, or text only mode;
Instead of just rotating letters, generates a pseudo-random ordered alphabet, and then substitutes.

## Do you want to help?

See my [TODO](https://gitlab.com/kresserbash/cipher-master/-/blob/master/TODO.md) list...

# Hashed Clone thanks you