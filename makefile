objects = main.o
bin = cipher

cipher : $(objects)
	cc -g -o $(bin) $(objects)

main.o : main.c
	cc -g -c main.c

build : cipher
	rm $(objects)

clean :
	rm $(bin) $(objects)
